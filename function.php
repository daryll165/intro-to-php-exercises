    <?php
        #Number 1 
        function fileLine($filename,$lineNum){
           if(file_exists($filename)){       
                $insideFile = fopen($filename,'r');       
                $count = 0;
                while(!feof($insideFile)){                  
                    $count++;                      
                    $lines = fgets($insideFile);      
                    if($count == $lineNum){
                        return "<center>$lines";
                    }             
                }
            }
            else
            {
                return "The file $insideFile does not exists!";
            }
        }


        #Number 2
        function appendLine($filename,$data,$lineNum)  {
            $fp = fopen($filename, "a");
            while (!feof($fp)) {
                fwrite($fp, $data);
                return $data;
            }   
        }                


        #Number 3 
        function randomLiner($filename){      
            $vowels = array('a','e','i','o','u');
            $len = strlen($filename);   
            $lines = file( $filename );     
            $countvow = 0;          
            for($i=0; $i<$len; $i++){
                if(in_array($filename[$i], $vowels)){
                    $countvow++;
                }
            }
            if ($countvow%2==0) {          
                echo "<br><center>".$lines[2]."<br>";
                echo $lines[4]."<br>";
            }
            if($countvow%2 !=0 ){
                echo $lines[1]."<br>";
                echo $lines[3]."<br>";
            } 
            if (file_exists($filename)) {
                echo "The file $filename exists";
            } else {
                echo "The file $filename does not exists!";
            }
        }


        #Number 4
        function testLoyalty($name){
            if(isset($_POST["submit"])){
                $nlen = strlen($name);
                $forE = substr_count($name,"E");
                $forA = substr_count($name,"A");
                $forN = substr_count($name,"N");
                $forTotal = $forE + $forA + $forN;
                if($forTotal>=3){
                    $result = $nlen * $forTotal;
                    if ($result % 6 == 0){
                        return "Loyal";
                    }else{
                        return "Di sure";
                    }
                }
                else{
                    return "Di sure";
                }
            }                                 
        }


        #Number 5
        function factorial(){
            if(isset($_POST["submit"])){
                $num = $_POST['number'];   
                if ($num<0){
                    $num = abs($num);
                        #Absolute Number
                    echo "<center>$num";
                }else{  
                    for ($ctr =$num-1;$ctr >0 ;$ctr--){
                        $num*=$ctr;
                    } 
                        #Factorial
                    echo "<center>$num";  
                }          
            }
        }


        #Number 6
        function primeCheck($number){
            if (isset($_POST['submit'])){
                if ($number == 1)
                return 0;
                for ($counter = 2; $counter <= $number/2; $counter++){
                    if ($number % $counter == 0)
                        return 0;
                }
                return 1;
            }
        }
        

        #Number 7
        function reverse(){
            if(isset($_POST["text"])){
                if($_POST){     
                    $textInput = $_POST['text'];     
                 
                 function reverseHalf($textInput){
                    $len = strlen($textInput);
                    if($len % 2 ==0){
                        $half = $len/2;
                        $halfRev = substr($textInput, -$half);
                        return  strrev($halfRev);
                    }
                    else{
                        return strrev($textInput);
                    }
                 }
                 echo "<h4> " .(reverseHalf($textInput));
                 
                 }
            }
        }    
       
?>