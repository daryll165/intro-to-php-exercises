<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 7</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<style>
    .text1{
        font-family: fantasy;
    }
    .style{
        background-color:grey;
    }
    .design{
        margin-left:33%;
    }
</style>
<body class = "style">
    <br><br>
    <center><h3 class = "text1">Text Reverser</h3>
    <br>
    <form class="row g-1 design" method = "post">
        <div class="col-auto">
            <input type="text" readonly class="form-control-plaintext" value="Input a text to reverse:">
        </div>
        <div class="col-auto">
            <input type="text" class="form-control"  placeholder="Enter a text.." name = "text">
        </div>
        <div class="col-auto">
            <button type="submit" name ="submit" class="btn btn-primary mb-3">Reverse Text</button>
        </div>
    </form>
    <?php     
        include_once("function.php"); 
        echo reverse();
    ?>
</body>
</html>                