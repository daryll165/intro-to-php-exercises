<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number 6</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<style>
    .text1{
        font-family: fantasy;
    }
    .style{
        background-color:grey;
    }
    .design{
        margin-left:33%;
    }
</style>
<body class = "style">
    <br><br>
    <center><h3 class = "text1">Prime Number Checker</h3>
    <br>
    <form class="row g-1 design" method = "post">
        <div class="col-auto">
            <input type="text" readonly class="form-control-plaintext" value="Input a number :">
        </div>
        <div class="col-auto">
            <input type="number" class="form-control"  placeholder="Enter a number.." name = "number">
        </div>
        <div class="col-auto">
            <button type="submit" name ="submit" class="btn btn-primary mb-3">Submit</button>
        </div>
    </form>
    <?php     
        include_once("function.php");
        if(isset($_POST["submit"])){
            $number = $_POST["number"];
            $checker = primeCheck($number);
            if ($checker == 1)
            echo $number." is a Prime Number";
            else
            echo $number." is not a Prime Number";
        }   
    ?>
</body>
</html>
